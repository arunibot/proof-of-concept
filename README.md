aRunibot aims to provide client-side robot capabilities to Ada software.

As robots may have to mimic web browsers, significant use of packet analyzers has been made to develop aRunibot. Mixed abstraction level units have been written.

An HTTP state management mechanism, compliant with RFC 6265:

This unit is built on top of the web development framework AWS (written by Dmitriy Anisimkov and Pascal Obry)

A secure tasking model:

This relies entirely on Ada concurrency features. A protected type with a barrier is defined to provide mutually exclusive access to an HTTP session, allowing multiple tasks to run concurrently within the same HTTP context. Should a task require consecutive accesses to the session, the barrier may be used.

A GNAT.Spitbol front-end:

GNAT.Spitbol allows SNOBOL-style pattern matching of type-0 languages in the Chomsky hierarchy (also known as recursively enumerable languages).

This unit provides assisted pattern construction and assisted pattern matching, avoiding the user from struggling with SNOBOL or introducing SNOBOL-related bugs into their code.

Development:

-Use restrictions provided by the Ravenscar profile. 
(PENDING)

-Generate an LALR(1) parser as an alternative to GNAT.Spitbol, by reusing previous work on AdaGOOP. 
(TODO)

-Use other code generation techniques to automatically construct SNOBOL-style patterns. 
(TODO)
