with AWS.Client.HTTP_Sessions;
with Ogame.Session_Datas;
with Ogame.Session_IDs;

package Ogame.Sessions is

   type Ogame_Session is new AWS.Client.HTTP_Sessions.HTTP_Session with
      record
         Session_Data : Ogame.Session_Datas.Session_Data;
         Session_ID   : Ogame.Session_IDs.Session_ID;
      end record;

   type Ogame_Session_Access is access Ogame_Session;

   function Get_Session_Data (Session : Ogame_Session) return String;
   procedure Set_Session_Data (Session : access Ogame_Session);

   function Get_Session_ID (Session : Ogame_Session) return String;
   procedure Set_Session_ID (Session : access Ogame_Session);

end Ogame.Sessions;
