with Ada.Strings.Unbounded;
with GNAT.Spitbol.Patterns;

package body Ogame.Watch is

   function Attack_Slider (Data : AWS.Response.Data) return Boolean is
   begin
      if GNAT.Spitbol.Patterns.Match
           (Ada.Strings.Unbounded.To_String
              (AWS.Response.Message_Body (Data)), "mySlider.open")
      then
         return True;
      else
         return False;
      end if;
   end Attack_Slider;

end Ogame.Watch;
