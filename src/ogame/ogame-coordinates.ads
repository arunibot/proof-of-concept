package Ogame.Coordinates is

   subtype Galaxy_Position is Integer range 1 .. 9;
   subtype System_Position is Integer range 1 .. 499;
   subtype Planet_Position is Integer range 1 .. 15;

   type Coords is
      record
         Galaxy : Galaxy_Position;
         System : System_Position;
         Planet : Planet_Position;
      end record;

end Ogame.Coordinates;
