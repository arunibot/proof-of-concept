package body Ogame.Session_IDs is

   function To_Session_ID (Str : String)
                          return Session_ID
   is
   begin
      return Session_ID'(ID => SB.To_Bounded_String (Str));
   end To_Session_ID;

   function To_String (ID : Session_ID)
                      return String
   is
   begin
      return SB.To_String (ID.ID);
   end To_String;

end Ogame.Session_IDs;
