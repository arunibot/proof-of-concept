with Ada.Strings.Bounded;

package Ogame.Session_IDs is

   package SB is new Ada.Strings.Bounded.Generic_Bounded_Length (Max => 64);

   type Session_ID is tagged
      record
         ID : SB.Bounded_String;
      end record;

   type Session_ID_Access is access Session_ID;

   function To_Session_ID (Str : String)
                          return Session_ID;

   function To_String (ID : Session_ID)
                      return String;

end Ogame.Session_IDs;
