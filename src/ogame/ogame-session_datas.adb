package body Ogame.Session_Datas is

   function To_Session_Data (Str  : String)
                            return Session_Data
   is
   begin
      return Session_Data'(Data => SB.To_Bounded_String (Str));
   end To_Session_Data;

   function To_String (Data : Session_Data)
                      return String
   is
   begin
      return SB.To_String (Data.Data);
   end To_String;

end Ogame.Session_Datas;
