with AWS.Response;

package Ogame.Buildings is

   type Resources_Level is
      record
         Metal     : Integer;
         Cristal   : Integer;
         Deuterium : Integer;
         Energy    : Integer;
      end record;

   type Type_De_Batiment is (Metal_Mine,
                             Cristal_Mine,
                             Deuterium_Mine,
                             Solar_Plant);

   type Building_State is (Building, Not_Building);

   function Code_De_Batiment (Batiment : Type_De_Batiment) return String;

   function Current_Building_State
              (Data : AWS.Response.Data) return Building_State;

   procedure Do_Not_Build (Data : AWS.Response.Data);

   procedure Get_Needed_Resources
               (Data : AWS.Response.Data;
                Needed_Resources_Level : out Resources_Level);

   procedure Get_Current_Resources
               (Data : AWS.Response.Data;
                Current_Resources_Level : out Resources_Level);

   procedure Get_Token_Value (Data : AWS.Response.Data;
                              Token : out String);

   function Has_Enough_Resources
              (Current_Resources_Level : Resources_Level;
               Needed_Resources_Level  : Resources_Level) return Boolean;

   procedure Put_Current_Building_Level (Data : AWS.Response.Data);

   procedure Put_Resources_Level (Level : Resources_Level);

   procedure Test (Data : AWS.Response.Data);

end Ogame.Buildings;
