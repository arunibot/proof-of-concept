with Ada.Containers.Vectors;
with Ogame.Coordinates;

package Ogame.Colonies is

   subtype Colony_ID is String (1 .. 8);
   subtype Colony_Name is String (1 .. 20);

   type Colony is
      record
         ID     : Colony_ID;
         Coords : Ogame.Coordinates.Coords;
         Name   : Colony_Name;
      end record;

   package Vectors is new Ada.Containers.Vectors (Positive, Colony);

end Ogame.Colonies;
