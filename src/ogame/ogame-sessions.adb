with Ada.Strings.Fixed;
with Ada.Strings.Unbounded;
with Ada.Text_IO;
with AWS.Headers;
with AWS.Response;

package body Ogame.Sessions is

   function Get_Session_Data (Session : Ogame_Session)
                             return String
   is
   begin
      return Session.Session_Data.To_String;
   end Get_Session_Data;

   function Get_Session_ID (Session : Ogame_Session)
                           return String
   is
   begin
      return Session.Session_ID.To_String;
   end Get_Session_ID;

   procedure Set_Session_Data (Session : access Ogame_Session)
   is
      Set_Cookie_Field : constant String := AWS.Response.Header (Session.Data)
                                           .Get_Values ("Set-Cookie");
      Location_Field   : constant String := AWS.Response.Header (Session.Data)
                                           .Get_Values ("Location");
      L : constant Natural := Location_Field'Length;
      I : constant Natural := Ada.Strings.Fixed.Index
                                (Source  => Location_Field,
                                 Pattern => "data=");
      S : constant String  := Ada.Strings.Fixed.Tail
                                (Source  => Location_Field,
                                 Count   => L - (I + 4),
                                 Pad     => Character'Val (32));
   begin
      Ada.Text_IO.Put_Line ("--------Header_List: Start--------");
      for I in 1 .. Session.Header_List.Length loop
         Ada.Text_IO.Put_Line (AWS.Headers.Get_Line (Session.Header_List, I));
      end loop;
      Ada.Text_IO.Put_Line ("--------Header_List: End--------");
      Ada.Text_IO.Put_Line ("Set-Cookie Field: " & Set_Cookie_Field);
      Ada.Text_IO.Put_Line ("Location Field: " & Location_Field);
      Ada.Text_IO.Put_Line ("--------Message body: Start--------");
      Ada.Text_IO.Put_Line (Ada.Strings.Unbounded.To_String
                              (AWS.Response.Message_Body (Session.Data)));
      Ada.Text_IO.Put_Line ("--------Message body: End--------");
      Ada.Text_IO.Put_Line ("Session Data: " & S);
      Session.Session_Data := Ogame.Session_Datas.To_Session_Data (S);
   end Set_Session_Data;

   procedure Set_Session_ID (Session : access Ogame_Session)
   is
      Set_Cookie_Field : constant String := AWS.Response.Header (Session.Data)
                                           .Get_Values ("Set-Cookie");
      Location_Field   : constant String := AWS.Response.Header (Session.Data)
                                           .Get_Values ("Location");
      L : constant Natural := Location_Field'Length;
      I : constant Natural := Ada.Strings.Fixed.Index
                                (Source  => Location_Field,
                                 Pattern => "PHPSESSID=");
      S : constant String  := Ada.Strings.Fixed.Tail
                                (Source  => Location_Field,
                                 Count   => L - (I + 9),
                                 Pad     => Character'Val (32));
   begin
      Ada.Text_IO.Put_Line ("--------Header_List: Start--------");
      for I in 1 .. Session.Header_List.Length loop
         Ada.Text_IO.Put_Line (AWS.Headers.Get_Line (Session.Header_List, I));
      end loop;
      Ada.Text_IO.Put_Line ("--------Header_List: End--------");
      Ada.Text_IO.Put_Line ("Set-Cookie Field: " & Set_Cookie_Field);
      Ada.Text_IO.Put_Line ("Location Field: " & Location_Field);
      Ada.Text_IO.Put_Line ("--------Message body: Start--------");
      Ada.Text_IO.Put_Line (Ada.Strings.Unbounded.To_String
                              (AWS.Response.Message_Body (Session.Data)));
      Ada.Text_IO.Put_Line ("--------Message body: End--------");
      Ada.Text_IO.Put_Line ("Session ID: " & S);
      Session.Session_ID := Ogame.Session_IDs.To_Session_ID (S);
   end Set_Session_ID;

end Ogame.Sessions;
