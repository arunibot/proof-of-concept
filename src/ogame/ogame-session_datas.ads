with Ada.Strings.Bounded;

package Ogame.Session_Datas is

   package SB is new Ada.Strings.Bounded.Generic_Bounded_Length (Max => 256);

   type Session_Data is tagged
      record
         Data : SB.Bounded_String;
      end record;

   type Session_Data_Access is access Session_Data;

   function To_Session_Data (Str  : String)
                            return Session_Data;

   function To_String (Data : Session_Data)
                      return String;

end Ogame.Session_Datas;
