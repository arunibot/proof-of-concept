with Ada.Strings.Unbounded;
with GNAT.Spitbol.Patterns;

with Utils.Screen;

package body Ogame.Buildings is

   use type GNAT.Spitbol.Patterns.Pattern;

   function Code_De_Batiment (Batiment : Type_De_Batiment) return String is
   begin
      case Batiment is
         when Metal_Mine =>
            return "1";
         when Cristal_Mine =>
            return "2";
         when Deuterium_Mine =>
            return "3";
         when Solar_Plant =>
            return "4";
      end case;
   end Code_De_Batiment;

   function Current_Building_State
              (Data : AWS.Response.Data) return Building_State is
      Building_Type : aliased GNAT.Spitbol.VString;
      Construc      : constant GNAT.Spitbol.Patterns.Pattern :=
                         "Interrompre la construction de " &
                         GNAT.Spitbol.Patterns.Arb * Building_Type &
                         " ?" &
                         GNAT.Spitbol.Patterns.Arb &
                         "queuePic";
   begin
      if GNAT.Spitbol.Patterns.Match
           (Ada.Strings.Unbounded.To_String
              (AWS.Response.Message_Body (Data)), Construc)
      then
         return Building;
      else
         return Not_Building;
      end if;
   end Current_Building_State;

   procedure Do_Not_Build (Data : AWS.Response.Data) is
      Building_Type : aliased GNAT.Spitbol.VString;
      Construc      : constant GNAT.Spitbol.Patterns.Pattern :=
                         "Interrompre la construction de " &
                         GNAT.Spitbol.Patterns.Arb * Building_Type &
                         " ?" &
                         GNAT.Spitbol.Patterns.Arb &
                         "queuePic";
   begin
      GNAT.Spitbol.Patterns.Match
        (Ada.Strings.Unbounded.To_String
           (AWS.Response.Message_Body (Data)), Construc);
      Utils.Screen.Put ("Batiment en construction: " &
                        Ada.Strings.Unbounded.To_String (Building_Type));
   end Do_Not_Build;

   procedure Get_Needed_Resources
               (Data : AWS.Response.Data;
                Needed_Resources_Level : out Resources_Level) is
      VNeeded_Metal     : aliased GNAT.Spitbol.VString;
      VNeeded_Cristal   : aliased GNAT.Spitbol.VString;
      VNeeded_Deuterium : aliased GNAT.Spitbol.VString;
      VNeeded_Energy    : aliased GNAT.Spitbol.VString;

      MM, CC, DD, EE : GNAT.Spitbol.Patterns.Match_Result;
      Dot                      : constant GNAT.Spitbol.Patterns.Pattern :=
                                    GNAT.Spitbol.Patterns.Any (".");
      Guillemet                : constant GNAT.Spitbol.Patterns.Pattern :=
                                    GNAT.Spitbol.Patterns.Any ('"');
      Digit_Dot_Pattern        : constant GNAT.Spitbol.Patterns.Pattern :=
                                    GNAT.Spitbol.Patterns.Span ("0123456789.");
      Needed_Metal_Pattern     : constant GNAT.Spitbol.Patterns.Pattern :=
                                    "tipsStandard" &
                                    Guillemet &
                                    " title=" &
                                    Guillemet &
                                    "|" &
                                    Digit_Dot_Pattern * VNeeded_Metal &
                                    " M";
      Needed_Cristal_Pattern   : constant GNAT.Spitbol.Patterns.Pattern :=
                                    "tipsStandard" &
                                    Guillemet &
                                    " title=" &
                                    Guillemet &
                                    "|" &
                                    Digit_Dot_Pattern * VNeeded_Cristal &
                                    " C";
      Needed_Deuterium_Pattern : constant GNAT.Spitbol.Patterns.Pattern :=
                                    "tipsStandard" &
                                    Guillemet &
                                    " title=" &
                                    Guillemet &
                                    "|" &
                                    Digit_Dot_Pattern * VNeeded_Deuterium &
                                    " D";
      Needed_Energy_Pattern    : constant GNAT.Spitbol.Patterns.Pattern :=
                                    "cessaire:" &
                                    GNAT.Spitbol.Patterns.Arb &
                                    "<span class=" &
                                    Guillemet &
                                    "time" &
                                    Guillemet &
                                    ">" &
                                    GNAT.Spitbol.Patterns.Arb &
                                    Digit_Dot_Pattern * VNeeded_Energy &
                                    GNAT.Spitbol.Patterns.Arb &
                                    "</span>";
   begin
      if GNAT.Spitbol.Patterns.Match
           (Ada.Strings.Unbounded.To_String
              (AWS.Response.Message_Body (Data)), Needed_Metal_Pattern)
      then
         GNAT.Spitbol.Patterns.Match (VNeeded_Metal, Dot, MM);
         GNAT.Spitbol.Patterns.Replace (MM, GNAT.Spitbol.Nul);
         Needed_Resources_Level.Metal :=
            Integer'Value (Ada.Strings.Unbounded.To_String (VNeeded_Metal));
      else
         Needed_Resources_Level.Metal := 0;
      end if;
      if GNAT.Spitbol.Patterns.Match
           (Ada.Strings.Unbounded.To_String
              (AWS.Response.Message_Body (Data)), Needed_Cristal_Pattern)
      then
         GNAT.Spitbol.Patterns.Match (VNeeded_Cristal, Dot, CC);
         GNAT.Spitbol.Patterns.Replace (CC, GNAT.Spitbol.Nul);
         Needed_Resources_Level.Cristal :=
            Integer'Value (Ada.Strings.Unbounded.To_String (VNeeded_Cristal));
      else
         Needed_Resources_Level.Cristal := 0; -- D�pot de M�tal par exemple
      end if;
      if GNAT.Spitbol.Patterns.Match
           (Ada.Strings.Unbounded.To_String
              (AWS.Response.Message_Body (Data)),
               Needed_Deuterium_Pattern)
      then
         GNAT.Spitbol.Patterns.Match (VNeeded_Deuterium, Dot, DD);
         GNAT.Spitbol.Patterns.Replace (DD, GNAT.Spitbol.Nul);
         Needed_Resources_Level.Deuterium :=
            Integer'Value
               (Ada.Strings.Unbounded.To_String (VNeeded_Deuterium));
      else
         Needed_Resources_Level.Deuterium := 0;
      end if;
      if GNAT.Spitbol.Patterns.Match
           (Ada.Strings.Unbounded.To_String
              (AWS.Response.Message_Body (Data)), Needed_Energy_Pattern)
      then
         GNAT.Spitbol.Patterns.Match (VNeeded_Energy, Dot, EE);
         GNAT.Spitbol.Patterns.Replace (EE, GNAT.Spitbol.Nul);
         Needed_Resources_Level.Energy :=
            Integer'Value (Ada.Strings.Unbounded.To_String (VNeeded_Energy));
      else
         Needed_Resources_Level.Energy := 0;
      end if;
   end Get_Needed_Resources;

   ----------------------------------------------------------------------------
   --  Attention: Dans la proc�dure suivante Get_Current_Resources,          --
   --  les if sont tr�s important car ils permettent d'emp�cher              --
   --  certains freezes lorsque Match �choue. Ex: Match laissant             --
   --  la chaine VString vide, une instruction du type                       --
   --                                                                        --
   --  Current_Resources_Level.Energy :=                                     --
   --     Integer'Value(Ada.Strings.Unbounded.To_String(Energy_Resources));  --
   --                                                                        --
   --  est bloquante.                                                        --
   ----------------------------------------------------------------------------
   procedure Get_Current_Resources
               (Data : AWS.Response.Data;
                Current_Resources_Level : out Resources_Level) is
      Metal_Resources     : aliased GNAT.Spitbol.VString;
      Cristal_Resources   : aliased GNAT.Spitbol.VString;
      Deuterium_Resources : aliased GNAT.Spitbol.VString;
      Energy_Resources    : aliased GNAT.Spitbol.VString;

      MM, CC, DD, EE : GNAT.Spitbol.Patterns.Match_Result;

      Dot                : constant GNAT.Spitbol.Patterns.Pattern :=
                              GNAT.Spitbol.Patterns.Any (".");
      Digit_Dot_Pattern  : constant GNAT.Spitbol.Patterns.Pattern :=
                              GNAT.Spitbol.Patterns.Span ("0123456789.");
      Digit_Dash_Pattern : constant GNAT.Spitbol.Patterns.Pattern :=
                              GNAT.Spitbol.Patterns.Span ("0123456789.-");
      Metal_Pattern      : constant GNAT.Spitbol.Patterns.Pattern :=
                              "resources_metal" &
                              GNAT.Spitbol.Patterns.Arb &
                              Digit_Dot_Pattern * Metal_Resources &
                              GNAT.Spitbol.Patterns.Arb &
                              "crystal_box";
      Cristal_Pattern    : constant GNAT.Spitbol.Patterns.Pattern :=
                              "resources_crystal" &
                              GNAT.Spitbol.Patterns.Arb &
                              Digit_Dot_Pattern * Cristal_Resources &
                              GNAT.Spitbol.Patterns.Arb &
                              "deuterium_box";
      Deuterium_Pattern  : constant GNAT.Spitbol.Patterns.Pattern :=
                              "resources_deuterium" &
                              GNAT.Spitbol.Patterns.Arb &
                              Digit_Dot_Pattern * Deuterium_Resources &
                              GNAT.Spitbol.Patterns.Arb &
                              "energy_box";
      Energy_Pattern     : constant GNAT.Spitbol.Patterns.Pattern :=
                              "resources_energy" &
                              GNAT.Spitbol.Patterns.Arb &
                              Digit_Dash_Pattern * Energy_Resources &
                              --  Energy may be negative.
                              GNAT.Spitbol.Patterns.Arb &
                              "darkmatter_box";
      Matching_Error : exception;
   begin
      if GNAT.Spitbol.Patterns.Match
           (Ada.Strings.Unbounded.To_String
              (AWS.Response.Message_Body (Data)), Metal_Pattern)
      then
         GNAT.Spitbol.Patterns.Match (Metal_Resources, Dot, MM);
         GNAT.Spitbol.Patterns.Replace (MM, GNAT.Spitbol.Nul);
         Current_Resources_Level.Metal :=
            Integer'Value (Ada.Strings.Unbounded.To_String (Metal_Resources));
      else
         raise Matching_Error;
      end if;
      if GNAT.Spitbol.Patterns.Match
           (Ada.Strings.Unbounded.To_String
              (AWS.Response.Message_Body (Data)), Cristal_Pattern)
      then
         GNAT.Spitbol.Patterns.Match (Cristal_Resources, Dot, CC);
         GNAT.Spitbol.Patterns.Replace (CC, GNAT.Spitbol.Nul);
         Current_Resources_Level.Cristal :=
            Integer'Value
               (Ada.Strings.Unbounded.To_String (Cristal_Resources));
      else
         raise Matching_Error;
      end if;

      if GNAT.Spitbol.Patterns.Match
           (Ada.Strings.Unbounded.To_String
              (AWS.Response.Message_Body (Data)), Deuterium_Pattern)
      then
         GNAT.Spitbol.Patterns.Match (Deuterium_Resources, Dot, DD);
         GNAT.Spitbol.Patterns.Replace (DD, GNAT.Spitbol.Nul);
         Current_Resources_Level.Deuterium :=
            Integer'Value
               (Ada.Strings.Unbounded.To_String (Deuterium_Resources));
      else
         raise Matching_Error;
      end if;

      if GNAT.Spitbol.Patterns.Match
           (Ada.Strings.Unbounded.To_String
              (AWS.Response.Message_Body (Data)), Energy_Pattern)
      then
         GNAT.Spitbol.Patterns.Match (Energy_Resources, Dot, EE);
         GNAT.Spitbol.Patterns.Replace (EE, GNAT.Spitbol.Nul);
         Current_Resources_Level.Energy :=
            Integer'Value (Ada.Strings.Unbounded.To_String (Energy_Resources));
      else
         raise Matching_Error;
      end if;
   end Get_Current_Resources;

   procedure Get_Token_Value (Data  : AWS.Response.Data;
                              Token : out String) is
      Token_VString : aliased GNAT.Spitbol.VString;
      Guillemet     : constant GNAT.Spitbol.Patterns.Pattern :=
                         GNAT.Spitbol.Patterns.Any ("'");
      Length32      : constant GNAT.Spitbol.Patterns.Pattern :=
                         GNAT.Spitbol.Patterns.Len (32);
      Token_Pattern : constant GNAT.Spitbol.Patterns.Pattern :=
                         "name='token' value=" &
                         Guillemet & Length32 * Token_VString &
                         Guillemet;
   begin
      if GNAT.Spitbol.Patterns.Match
           (Ada.Strings.Unbounded.To_String
              (AWS.Response.Message_Body (Data)), Token_Pattern)
      then
         Token := Ada.Strings.Unbounded.To_String (Token_VString);
         Utils.Screen.Put ("Token: " &
                           Ada.Strings.Unbounded.To_String (Token_VString));
      else
         Utils.Screen.Put ("Token Unmatched!");
      end if;
   end Get_Token_Value;

   function Has_Enough_Resources
              (Current_Resources_Level : Resources_Level;
               Needed_Resources_Level  : Resources_Level) return Boolean is
   begin
      if Current_Resources_Level.Metal < Needed_Resources_Level.Metal
      or else
         Current_Resources_Level.Cristal < Needed_Resources_Level.Cristal
      or else
         Current_Resources_Level.Deuterium < Needed_Resources_Level.Deuterium
      then
         return False;
      else
         return True;
      end if;

   end Has_Enough_Resources;

   procedure Put_Current_Building_Level (Data : AWS.Response.Data) is
      Metal_Level     : aliased GNAT.Spitbol.VString;
      Cristal_Level   : aliased GNAT.Spitbol.VString;
      Deuterium_Level : aliased GNAT.Spitbol.VString;
      Centrale_Level  : aliased GNAT.Spitbol.VString;
      --  Test_String     : aliased GNAT.Spitbol.VString;
      Digit_Pattern     : constant GNAT.Spitbol.Patterns.Pattern :=
                             GNAT.Spitbol.Patterns.Span ("0123456789");
      Metal_Pattern     : constant GNAT.Spitbol.Patterns.Pattern :=
                             "Mine de m" &
                             GNAT.Spitbol.Patterns.Arb &
                             "tal :" &
                             GNAT.Spitbol.Patterns.Arb &
                             "au niveau " &
                             Digit_Pattern * Metal_Level;
      Cristal_Pattern   : constant GNAT.Spitbol.Patterns.Pattern :=
                             "Mine de cristal :" &
                             GNAT.Spitbol.Patterns.Arb &
                             "au niveau " &
                             Digit_Pattern * Cristal_Level;
      Deuterium_Pattern : constant GNAT.Spitbol.Patterns.Pattern :=
                             "tiseur" &
                             GNAT.Spitbol.Patterns.Arb &
                             "rium :" &
                             GNAT.Spitbol.Patterns.Arb &
                             "au niveau " &
                             Digit_Pattern * Deuterium_Level;
      Centrale_Pattern  : constant GNAT.Spitbol.Patterns.Pattern :=
                             "Centrale" &
                             GNAT.Spitbol.Patterns.Arb &
                             "solaire :" &
                             GNAT.Spitbol.Patterns.Arb &
                             "au niveau " &
                             Digit_Pattern * Centrale_Level;
   begin
      GNAT.Spitbol.Patterns.Match
        (Ada.Strings.Unbounded.To_String
           (AWS.Response.Message_Body (Data)), Metal_Pattern);
      Utils.Screen.Put ("Metal_Level: " &
                        Ada.Strings.Unbounded.To_String (Metal_Level));
      GNAT.Spitbol.Patterns.Match
        (Ada.Strings.Unbounded.To_String
           (AWS.Response.Message_Body (Data)), Cristal_Pattern);
      Utils.Screen.Put ("Cristal_Level: " &
                        Ada.Strings.Unbounded.To_String (Cristal_Level));
      GNAT.Spitbol.Patterns.Match
        (Ada.Strings.Unbounded.To_String
           (AWS.Response.Message_Body (Data)), Deuterium_Pattern);
      Utils.Screen.Put ("Deuterium_Level: " &
                        Ada.Strings.Unbounded.To_String (Deuterium_Level));
      GNAT.Spitbol.Patterns.Match
        (Ada.Strings.Unbounded.To_String
           (AWS.Response.Message_Body (Data)), Centrale_Pattern);
      Utils.Screen.Put ("Centrale_Level: " &
                        Ada.Strings.Unbounded.To_String (Centrale_Level));
   end Put_Current_Building_Level;

   procedure Put_Resources_Level (Level : Resources_Level) is
   begin
      Utils.Screen.Put ("Metal:" &
                        Integer'Image (Level.Metal) &
                        ", Cristal:" &
                        Integer'Image (Level.Cristal) &
                        ", Deuterium:" &
                        Integer'Image (Level.Deuterium) &
                        ", Energy:" &
                        Integer'Image (Level.Energy));
   end Put_Resources_Level;

   procedure Test (Data : AWS.Response.Data) is
   begin
      Utils.Screen.Put
         (Ada.Strings.Unbounded.To_String (AWS.Response.Message_Body (Data)));
   end Test;

end Ogame.Buildings;
