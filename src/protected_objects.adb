with Ada.Text_IO;
with AWS.Client;
with AWS.Headers.Set;
with AWS.Messages;
with Ogame.Configuration;
with AWS.Headers;

package body Protected_Objects is

   protected body Protected_Connection_Type is

      entry Login_00 (User_Agent : String)
        when not Barrier_Released
      is
      begin
         Protected_Session.Create
           (URL        => "http://fr.ogame.gameforge.com",
            User_Agent => User_Agent);

         Protected_Session.Get;

         AWS.Headers.Set.Add
           (Headers => Protected_Session.Header_List,
            Name    => AWS.Messages.Referer_Token,
            Value   => "http://fr.ogame.gameforge.com/");
      end Login_00;

      procedure Login_01 (User_Agent : String)
      is
      begin

         Protected_Session.Post
           (Data         => "kid=&uni=" &
                            Ogame.Configuration.Uni_Hostname &
                            "&login=" &
                            Ogame.Configuration.Username &
                            "&pass=" &
                            Ogame.Configuration.Password,
            Content_Type => "application/x-www-form-urlencoded",
            URI          => "/main/login");

         Protected_Session.Set_Session_Data;

         Protected_Session.Create
           (URL        => "http://" & Ogame.Configuration.Uni_Hostname,
            User_Agent => User_Agent);

         Protected_Session.Get
           (URI => "/game/reg/login2.php?data=" &
                   Protected_Session.Get_Session_Data);

         Protected_Session.Set_Session_ID;

         Protected_Session.Get
           (URI => "/game/index.php?page=overview&PHPSESSID=" &
                   Protected_Session.Get_Session_ID);

         AWS.Headers.Set.Update
           (Headers => Protected_Session.Header_List,
            Name    => AWS.Messages.Referer_Token,
            Value   => AWS.Client.Host (Protected_Session.Connection) &
                       "/game/index.php?page=overview&PHPSESSID=" &
                       Protected_Session.Get_Session_ID);
      end Login_01;

      procedure Login_02 (Data : out AWS.Response.Data) is
      begin
         Protected_Session.Get
           (URI => "/game/index.php?page=overview");

         AWS.Headers.Set.Update
           (Headers => Protected_Session.Header_List,
            Name    => AWS.Messages.Referer_Token,
            Value   => AWS.Client.Host (Protected_Session.Connection) &
                       "/game/index.php?page=overview");

         Data := Protected_Session.Data;
      end Login_02;

      entry Watch_00 (Data : out AWS.Response.Data)
        when Barrier_Released is
      begin
         Protected_Session.Get
           (URI => "/game/index.php?page=overview");

         AWS.Headers.Set.Update
           (Headers => Protected_Session.Header_List,
            Name    => AWS.Messages.Referer_Token,
            Value   => AWS.Client.Host (Protected_Session.Connection) &
                       "/game/index.php?page=overview");

         Data := Protected_Session.Data;
         Barrier_Released := False;
         --  Last statement provides consecutiveness of
         --  Watch_Task protected procedures.
      end Watch_00;

      entry Building_00 (Data : out AWS.Response.Data)
        when Barrier_Released is
      begin
         Protected_Session.Get
           (URI => "/game/index.php?page=resources");

         AWS.Headers.Set.Update
           (Headers => Protected_Session.Header_List,
            Name    => AWS.Messages.Referer_Token,
            Value   => AWS.Client.Host (Protected_Session.Connection) &
                       "/game/index.php?page=resources");
         Data := Protected_Session.Data;
         Barrier_Released := False;
         --  Last statement provides consecutiveness of
         --  Building_Task protected procedures.
      end Building_00;

      procedure Building_01
                  (Data          : out AWS.Response.Data;
                   Building_Type : String) is
      begin
         Protected_Session.Post
           (Data         => "type=" & Building_Type,
            Content_Type => "application/x-www-form-urlencoded",
            URI          => "/game/index.php?page=resources" &
                            "&ajax=1");

         Data := Protected_Session.Data;
      end Building_01;

      procedure Building_02 (Token         : String;
                             Building_Type : String) is
      begin
         Protected_Session.Post
           (Data         => "token=" &
                            Token &
                            "&modus=1&type=" &
                            Building_Type,
            Content_Type => "application/x-www-form-urlencoded",
            URI          => "/game/index.php?page=resources");
      end Building_02;

      entry Test_00
        when Barrier_Released is
      begin
         Barrier_Released := False;
         --  Last statement provides consecutiveness of
         --  Test_Task protected procedures.
      end Test_00;

      procedure Test_01 is
      begin
         null;
      end Test_01;

      procedure Test_02 is
      begin
         null;
      end Test_02;

      procedure Release_Barrier (Releaser : String) is
      begin
         Ada.Text_IO.Put_Line (Releaser & ": Barrier Released!");
         Barrier_Released := True;
      end Release_Barrier;

   end Protected_Connection_Type;

end Protected_Objects;
