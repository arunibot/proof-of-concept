with Protected_Objects;
--  Provides instances of the following tasks with shared connections.

with Login_Task;
with Watch_Task;
with Building_Task;
--  with Test_Task;

procedure Main is
   Protected_Connection_Access :
      constant Protected_Objects.Protected_Connection_Access_Type :=
         new Protected_Objects.Protected_Connection_Type;

   Login_Task_Instance :
      Login_Task.Login_Task_Type
        (Protected_Connection => Protected_Connection_Access);

   Watch_Task_Instance :
      Watch_Task.Watch_Task_Type
        (Protected_Connection => Protected_Connection_Access);

   Building_Task_Instance :
      Building_Task.Building_Task_Type
        (Protected_Connection => Protected_Connection_Access);

   --  Test_Task_Instance :
   --     Test_Task.Test_Task_Type
   --       (Protected_Connection => Protected_Connection_Access);
begin

   null;

end Main;
