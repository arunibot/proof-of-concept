with AWS.Response;
with Ogame.Sessions; use Ogame.Sessions;

package Protected_Objects is

   protected type Protected_Connection_Type is
      entry     Login_00 (User_Agent : String);
      procedure Login_01 (User_Agent : String);
      procedure Login_02 (Data : out AWS.Response.Data); -- Grabs basic data

      entry Watch_00 (Data : out AWS.Response.Data);

      entry Building_00 (Data : out AWS.Response.Data);
      procedure Building_01
                  (Data          : out AWS.Response.Data;
                   Building_Type : String);
      procedure Building_02
                  (Token : String;
                   Building_Type : String);

      entry Test_00;
      procedure Test_01;
      procedure Test_02;

      procedure Release_Barrier (Releaser : String);
   private
      Protected_Session : Ogame_Session_Access := new Ogame_Session;
      Barrier_Released  : Boolean := False;
   end Protected_Connection_Type;

   type Protected_Connection_Access_Type is access Protected_Connection_Type;

end Protected_Objects;
