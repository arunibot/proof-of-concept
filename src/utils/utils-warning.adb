with Ada.Text_IO;
with GNAT.OS_Lib;
with Configuration;

package body Utils.Warning is

   procedure Sound_Warning
   is
      Player       : constant String := Configuration.Player;
      File_To_Play : constant String := Configuration.File_To_Play;
      Arg_List : constant GNAT.OS_Lib.Argument_List (1 .. 2) :=
                   (1 => new String'(Player (Player'First .. Player'Last)),
                    2 => new String'(File_To_Play (File_To_Play'First ..
                                                   File_To_Play'Last)));
      Arg_List_Access : GNAT.OS_Lib.Argument_List_Access :=
                           new GNAT.OS_Lib.Argument_List'(Arg_List (1 .. 2));
      Success : Boolean;
      Return_Code : Integer;
   begin
      GNAT.OS_Lib.Spawn
           (Program_Name    => Arg_List_Access (Arg_List_Access'First).all,
            Args            => Arg_List_Access (Arg_List_Access'First + 1 ..
                                                Arg_List_Access'Last),
            Output_File     => "Sound_Warning.Out",
            Success         => Success,
            Return_Code     => Return_Code);
      GNAT.OS_Lib.Free (Arg_List_Access);
   end Sound_Warning;

   procedure Text_Warning is
   begin
      Ada.Text_IO.New_Line;
      Ada.Text_IO.Put_Line ("Warning !!!");
      Ada.Text_IO.New_Line;
   end Text_Warning;

end Utils.Warning;
