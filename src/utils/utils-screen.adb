with Ada.Text_IO;
package body Utils.Screen is

   procedure Put (String_To_Be_Put : String) is
   begin
      Ada.Text_IO.Put_Line (String_To_Be_Put);
      Ada.Text_IO.New_Line;
   end Put;

end Utils.Screen;
