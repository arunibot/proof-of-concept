with AWS.Response;

with Utils.Screen;
use  Utils.Screen;

with Utils.Warning;

with Ogame.Watch;
use  Ogame.Watch;

package body Watch_Task is

   task body Watch_Task_Type is

      Task_Data : AWS.Response.Data;

   begin
      Put ("Watch : started...");
      Endless_Loop : loop
         Put ("Executing Watch_00 ...");
         Protected_Connection.Watch_00 (Data => Task_Data);
         if Attack_Slider (Task_Data) then
            Utils.Warning.Text_Warning;
            Utils.Warning.Sound_Warning;
         else
            Put ("As calm as a toad in the sun...");
         end if;
         delay (2.0);
         Put ("Sleeping 10m...");
         Protected_Connection.Release_Barrier ("Watch_Task");
         delay Duration (600.0);
      end loop Endless_Loop;
   end Watch_Task_Type;

end Watch_Task;
