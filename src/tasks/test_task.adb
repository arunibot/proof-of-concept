with Ada.Text_IO;

package body Test_Task is

   task body Test_Task_Type is
   begin
      Ada.Text_IO.Put_Line("Test_Task: started...");
      Endless_Loop: loop
         Protected_Connection.Test_00;
         -- Bla bla bla
         Protected_Connection.Test_01;
         -- Bla bla bla
         Protected_Connection.Test_02;
         -- Bla bla bla
         delay (2.0);
         Ada.Text_IO.Put_Line("Test_Task: sleeping 1s");
         Protected_Connection.Release_Barrier ("Test_Task");
         delay Duration(1);
      end loop Endless_Loop;
   end Test_Task_Type;

end Test_Task;
