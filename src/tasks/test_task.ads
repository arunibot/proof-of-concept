with Protected_Objects;

package Test_Task is

   task type Test_Task_Type (Protected_Connection : Protected_Objects.Protected_Connection_Access_Type);

end Test_Task;
