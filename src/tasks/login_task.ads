with Protected_Objects;

package Login_Task is

   task type Login_Task_Type
               (Protected_Connection :
                   Protected_Objects.Protected_Connection_Access_Type);

end Login_Task;
