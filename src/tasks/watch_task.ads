with Protected_Objects;

package Watch_Task is

   task type Watch_Task_Type
     (Protected_Connection :
         Protected_Objects.Protected_Connection_Access_Type);

end Watch_Task;
