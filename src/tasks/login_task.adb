with AWS.Configuration;
with AWS.Response;

--  with GNAT.Spitbol.Patterns; TODO: Colonies

with Utils.Screen;
use  Utils.Screen;

--  with Ogame.Colonies;

package body Login_Task is

   task body Login_Task_Type is

      Task_Data : AWS.Response.Data;

   begin
      Put ("Login_00 ...");
      Protected_Connection.Login_00
        (User_Agent => AWS.Configuration.User_Agent);
      delay Duration (3.0);

      Put ("Login_01 ...");
      Protected_Connection.Login_01
        (User_Agent => AWS.Configuration.User_Agent);
      delay Duration (2.0);

      Put ("Login_02 ...");
      Protected_Connection.Login_02 (Data => Task_Data);
      delay Duration (2.0);

      Protected_Connection.Release_Barrier ("Login_Task");
   end Login_Task_Type;

end Login_Task;
