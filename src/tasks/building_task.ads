with Protected_Objects;

package Building_Task is

   task type Building_Task_Type
               (Protected_Connection :
                   Protected_Objects.Protected_Connection_Access_Type);

end Building_Task;
