with AWS.Response;
with Ogame.Buildings; use Ogame.Buildings;
with Utils.Screen;    use Utils.Screen;

package body Building_Task is

   task body Building_Task_Type is

      Task_Data : AWS.Response.Data;

      Token : String (1 .. 32);

      Current_Resources_Level : Resources_Level;
      Needed_Resources_Level  : Resources_Level;

      procedure Open_Building_Slide (Batiment : Type_De_Batiment);
      procedure Open_Building_Slide (Batiment : Type_De_Batiment) is
      begin
         Protected_Connection.Building_01
           (Data          => Task_Data,
            Building_Type => Code_De_Batiment (Batiment));
      end Open_Building_Slide;

      Iterator : Integer := 0;
      --  Loop iterator for constructions;

   begin
      Put ("Building : started...");
      Endless_Loop : loop
         Put ("Executing Building ...");
         Protected_Connection.Building_00 (Task_Data);

         if Current_Building_State (Task_Data) = Building then
            Do_Not_Build (Task_Data);
         else
            Put ("No current construction.");
            Get_Token_Value (Task_Data, Token);
            Get_Current_Resources (Task_Data, Current_Resources_Level);
            Put_Resources_Level (Current_Resources_Level);
            --  Unknown (Task_Data);
            delay Duration (1.0);
            case Iterator rem 3 is
            when 0 =>
               Open_Building_Slide (Solar_Plant);
               Get_Needed_Resources (Task_Data, Needed_Resources_Level);
               Put_Resources_Level (Needed_Resources_Level);
               if Has_Enough_Resources (Current_Resources_Level,
                                        Needed_Resources_Level)
               then
                  delay Duration (3.0);
                  Protected_Connection.Building_02
                    (Token         => Token,
                     Building_Type => Code_De_Batiment (Solar_Plant));
                  Iterator := Iterator + 1;
               else
                  Put ("Not enough resources to build: " &
                       Type_De_Batiment'Image (Solar_Plant));
               end if;
            when 1 =>
               Open_Building_Slide (Cristal_Mine);
               Get_Needed_Resources (Task_Data, Needed_Resources_Level);
               Put_Resources_Level (Needed_Resources_Level);
               if Has_Enough_Resources (Current_Resources_Level,
                                        Needed_Resources_Level)
               then
                  delay Duration (3.0);
                  Protected_Connection.Building_02
                    (Token         => Token,
                     Building_Type => Code_De_Batiment (Cristal_Mine));
                  Iterator := Iterator + 1;
               else
                  Put ("Not enough resources to build: " &
                       Type_De_Batiment'Image (Cristal_Mine));
               end if;
            when 2 =>
               Open_Building_Slide (Metal_Mine);
               Get_Needed_Resources (Task_Data, Needed_Resources_Level);
               Put_Resources_Level (Needed_Resources_Level);
               if Has_Enough_Resources (Current_Resources_Level,
                                        Needed_Resources_Level)
               then
                  delay Duration (3.0);
                  Protected_Connection.Building_02
                    (Token         => Token,
                     Building_Type => Code_De_Batiment (Metal_Mine));
                  Iterator := Iterator + 1;
               else
                  Put ("Not enough resources to build: " &
                       Type_De_Batiment'Image (Metal_Mine));
               end if;
            when others =>
               Put ("Iterator error!");
            end case;
         end if;
         delay (2.0);
         Put ("Sleeping 300 secondes...");
         Protected_Connection.Release_Barrier ("Building_Task");
         delay Duration (300.0);
      end loop Endless_Loop;
   end Building_Task_Type;

end Building_Task;
