with Ada.Containers.Indefinite_Vectors;

package AWS.Headers.Values.Data_Vectors
   is new Ada.Containers.Indefinite_Vectors
            (Positive, AWS.Headers.Values.Data);
