with AWS.Client;
with AWS.Headers.Values.Data_Vectors; use AWS.Headers.Values.Data_Vectors;

package AWS.Cookie_Vectors is

   type Vector is
           new AWS.Headers.Values.Data_Vectors.Vector with null record;

   function To_Header_Line (Cookies : in Vector) return String;

   procedure Update (Cookies          : in out Vector;
                     Response_Headers : in     AWS.Client.Header_List);

   function Vectorize_Cookie_Header (Headers : in AWS.Client.Header_List)
                                    return Vector;

end AWS.Cookie_Vectors;
