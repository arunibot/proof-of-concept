with Ada.Strings.Fixed;
with AWS.Cookie_Vectors;
with AWS.Headers.Set;
with AWS.Messages;

package body AWS.Client.HTTP_Sessions is

   ------------
   -- Create --
   ------------

   procedure Create
     (Session     : in out HTTP_Session;
      URL         : in     String;
      User        : in     String := AWS.Client.No_Data;
      Pwd         : in     String := AWS.Client.No_Data;
      Proxy       : in     String := AWS.Client.No_Data;
      Proxy_User  : in     String := AWS.Client.No_Data;
      Proxy_Pwd   : in     String := AWS.Client.No_Data;
      Retry       : in     Natural := AWS.Client.Retry_Default;
      Persistent  : in     Boolean := True;
      Timeouts    : in     AWS.Client.Timeouts_Values := AWS.Client.No_Timeout;
      Server_Push : in     Boolean := False;
      Certificate : in     String  := AWS.Default.Client_Certificate;
      User_Agent  : in     String  := AWS.Default.User_Agent)
   --  New parameter name URL for Host, as Host turns
   --  out to be a full URL in AWS.Client.Create definition.
   is
   begin
      AWS.Headers.Set.Update
        (Headers => Session.Header_List,
         Name    => "Keep-Alive",
         Value   => "300");
      AWS.Headers.Set.Update
        (Headers => Session.Header_List,
         Name    => AWS.Messages.Host_Token,
         Value   => Ada.Strings.Fixed.Delete (Source  => URL,
                                              From    => 1,
                                              Through => 7));
      AWS.Headers.Set.Update
        (Headers => Session.Header_List,
         Name    => AWS.Messages.User_Agent_Token,
         Value   => User_Agent);
      AWS.Headers.Set.Update
        (Headers => Session.Header_List,
         Name    => AWS.Messages.Accept_Token,
         Value   => "Accept=text/html,application/xhtml+xml," &
                    "application/xml;q=0.9,*/*;q=0.8");
      AWS.Headers.Set.Update
        (Headers => Session.Header_List,
         Name    => AWS.Messages.Accept_Language_Token,
         Value   => "en-us,en;q=0.5");
      AWS.Headers.Set.Update
        (Headers => Session.Header_List,
         Name    => AWS.Messages.Accept_Encoding_Token,
         Value   => "gzip,deflate");
      AWS.Headers.Set.Update
        (Headers => Session.Header_List,
         Name    => AWS.Messages.Accept_Charset_Token,
         Value   => "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
      AWS.Client.Create (Connection  => Session.Connection,
                         Host        => URL,
                         User        => User,
                         Pwd         => Pwd,
                         Proxy       => Proxy,
                         Proxy_User  => Proxy_User,
                         Proxy_Pwd   => Proxy_Pwd,
                         Retry       => Retry,
                         Persistent  => Persistent,
                         Timeouts    => Timeouts,
                         Server_Push => Server_Push,
                         Certificate => Certificate,
                         User_Agent  => User_Agent);
   end Create;

   ---------
   -- Get --
   ---------

   procedure Get
     (Session    : in out HTTP_Session;
      URI        : in     String := No_Data;
      Data_Range : in     Content_Range := No_Range)

   is
      Cookies          : AWS.Cookie_Vectors.Vector;
      Response_Headers : AWS.Client.Header_List;
   begin
      Cookies := AWS.Cookie_Vectors.Vectorize_Cookie_Header
                                      (Session.Header_List);
      AWS.Client.Get (Connection => Session.Connection,
                      Result     => Session.Data,
                      Headers    => Session.Header_List,
                      URI        => URI,
                      Data_Range => Data_Range);
      Response_Headers := AWS.Response.Header (Session.Data);
      Cookies.Update (Response_Headers);
      AWS.Headers.Set.Update (Headers => Session.Header_List,
                              Name    => AWS.Messages.Cookie_Token,
                              Value   => Cookies.To_Header_Line);
   end Get;

   ----------
   -- Post --
   ----------

   procedure Post
     (Session      : in out HTTP_Session;
      Data         : in     String;
      Content_Type : in     String := No_Data;
      URI          : in     String := No_Data;
      Attachments  : in     Attachment_List := Empty_Attachment_List)
   is
      Cookies          : AWS.Cookie_Vectors.Vector;
      Response_Headers : AWS.Client.Header_List;
   begin
      Cookies := AWS.Cookie_Vectors.Vectorize_Cookie_Header
                                      (Session.Header_List);
      AWS.Client.Post (Connection   => Session.Connection,
                       Result       => Session.Data,
                       Headers      => Session.Header_List,
                       Data         => Data,
                       Content_Type => Content_Type,
                       URI          => URI,
                       Attachments  => Attachments);
      Response_Headers := AWS.Response.Header (Session.Data);
      Cookies.Update (Response_Headers);
      AWS.Headers.Set.Update (Headers => Session.Header_List,
                              Name    => AWS.Messages.Cookie_Token,
                              Value   => Cookies.To_Header_Line);
   end Post;

end AWS.Client.HTTP_Sessions;
