with Ada.Strings.Unbounded;
with AWS.Headers.Values;

package body AWS.Cookie_Vectors is

   --------------------
   -- To_Header_Line --
   --------------------

   function To_Header_Line (Cookies : in Vector) return String
   is
      L : constant Integer := Integer (Cookies.Length);
      S : Ada.Strings.Unbounded.Unbounded_String;
      use type Ada.Strings.Unbounded.Unbounded_String;
   begin
      if L = 0 then
         S := Ada.Strings.Unbounded.To_Unbounded_String ("");
      elsif L = 1 then
         S := Cookies.Element (1).Name &
              "=" &
              Cookies.Element (1).Value;
      else
         for I in Positive range 1 .. L - 1 loop
            S := S &
                 Cookies.Element (I).Name &
                 "=" &
                 Cookies.Element (I).Value &
                 "; ";
         end loop;
         S := S &
              Cookies.Element (L).Name &
              "=" &
              Cookies.Element (L).Value;
      end if;
      return Ada.Strings.Unbounded.To_String (S);
   end To_Header_Line;

   ------------
   -- Update --
   ------------

   procedure Update (Cookies          : in out Vector;
                     Response_Headers : in     AWS.Client.Header_List)
   is
      Cookie_Name_Is_New : Boolean;
      Cookies_String : constant String := AWS.Headers.Get_Values
                                            (Response_Headers, "Set-Cookie");
      Cookies_Set    : constant AWS.Headers.Values.Set :=
                                   AWS.Headers.Values.Split
                                     (Cookies_String);
      Cursor : AWS.Headers.Values.Data_Vectors.Cursor;
      use type Ada.Strings.Unbounded.Unbounded_String;
   begin
      for I in Cookies_Set'Range loop
         if Cookies_Set (I).Named_Value then
            if Cookies_Set (I).Name = "Domain" or
               Cookies_Set (I).Name = "domain"
            then
               null;
            elsif Cookies_Set (I).Name = "Expires" or
                  Cookies_Set (I).Name = "expires"
            then
               null;
            elsif Cookies_Set (I).Name = "Max-Age" or
                  Cookies_Set (I).Name = "max-age"
            then
               null;
            elsif Cookies_Set (I).Name = "Path" or
                  Cookies_Set (I).Name = "path"
            then
               null;
            elsif Cookies_Set (I).Name = "Path" or
                  Cookies_Set (I).Name = "path"
            then
               null;
            else
               Cookie_Name_Is_New := True;
               Cursor := Cookies.First;
               while Has_Element (Cursor) loop
                  if Element (Cursor).Name = Cookies_Set (I).Name
                  then
                     Cookie_Name_Is_New := False;
                     Cookies.Replace_Element
                       (Position => Cursor,
                        New_Item => Cookies_Set (I));
                     Cursor := Cookies.Last;
                  end if;
                  Next (Cursor);
               end loop;
               if Cookie_Name_Is_New
               then
                  Cookies.Append (Cookies_Set (I));
               end if;
            end if;
         else
         --  According to RFC 6265 4.1.1 Syntax, servers "SHOULD NOT"
         --  send Set-Cookie headers whose cookies do not begin with
         --  a name-value pair.
            if Cookies_Set (I).Value = "HttpOnly" or
               Cookies_Set (I).Value = "httponly"
            then
               null;
            elsif Cookies_Set (I).Value = "Secure" or
                  Cookies_Set (I).Value = "secure"
            then
               null;
            else
            --  Extension attribute or RFC 6265 infringement.
               null;
            end if;
         end if;
      end loop;
   end Update;

   -----------------------------
   -- Vectorize_Cookie_Header --
   -----------------------------

   function Vectorize_Cookie_Header (Headers : in AWS.Client.Header_List)
                                    return Vector
   is
      Cookies_String : constant String := AWS.Headers.Get_Values
                                            (Headers, "Cookie");
      Cookies_Set    : constant AWS.Headers.Values.Set :=
                                   AWS.Headers.Values.Split
                                     (Cookies_String);
      Cookies : Vector;
   begin
      for I in Cookies_Set'Range loop
         if Cookies_Set (I).Named_Value
         then
            Cookies.Append (Cookies_Set (I));
         else
         --  Extension attribute or RFC 6265 infringement.
            null;
         end if;
      end loop;
      return Cookies;
   end Vectorize_Cookie_Header;

end AWS.Cookie_Vectors;
