with AWS.Default;
with AWS.Response;

package AWS.Client.HTTP_Sessions is

   type HTTP_Session is tagged limited
      record
         Connection  : AWS.Client.HTTP_Connection;
         Data        : AWS.Response.Data;
         Header_List : AWS.Client.Header_List;
      end record;

   procedure Create
     (Session     : in out HTTP_Session;
      URL         : in     String;
      User        : in     String := AWS.Client.No_Data;
      Pwd         : in     String := AWS.Client.No_Data;
      Proxy       : in     String := AWS.Client.No_Data;
      Proxy_User  : in     String := AWS.Client.No_Data;
      Proxy_Pwd   : in     String := AWS.Client.No_Data;
      Retry       : in     Natural := AWS.Client.Retry_Default;
      Persistent  : in     Boolean := True;
      Timeouts    : in     AWS.Client.Timeouts_Values := AWS.Client.No_Timeout;
      Server_Push : in     Boolean := False;
      Certificate : in     String  := AWS.Default.Client_Certificate;
      User_Agent  : in     String  := AWS.Default.User_Agent);
   --  New parameter name URL for Host, as Host turns
   --  out to be a full URL in AWS.Client.Create definition

   procedure Get
     (Session    : in out HTTP_Session;
      URI        : in     String := No_Data;
      Data_Range : in     Content_Range := No_Range);

   procedure Post
     (Session      : in out HTTP_Session;
      Data         : in     String;
      Content_Type : in     String := No_Data;
      URI          : in     String := No_Data;
      Attachments  : in     Attachment_List := Empty_Attachment_List);

end AWS.Client.HTTP_Sessions;
